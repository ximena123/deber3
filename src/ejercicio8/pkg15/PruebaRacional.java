/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejercicio8.pkg15;
import java.text.DecimalFormat;
import java.util.Scanner;
/**
 *
 * @author Ximena Puchaicela
 */
class Racional{
    private static double numerador;
    private static double denominador;
    public Racional(double numerador,double denominador){
        Racional.numerador= numerador;
        Racional.denominador= denominador;
        reducir();
    }
    public static double getNumerador() {
        return numerador;
    }
    public static void setNumerador(double numerador) {
        Racional.numerador = numerador;
    }
    public static double getDenominador() {
        return denominador;
    }
    public static void setDenominador(double denominador) {
        Racional.denominador = denominador;
    }  
    public static void reducir(){
        double num=numerador;
        double den= denominador;
        double contador=(num<den)? Math.abs(num):Math.abs(den);
        for (int i = 1; i <= contador; i++) {
            if(num%i==0&& den%i==0){
                Racional.numerador=num/i;
                Racional.denominador= den/i;
            }
        }
    }
    public static void suma(int num1,int den1,int num2,int den2){
        numerador= ((den1*num2)+den2*num1);
        denominador=(den1*den2);
         reducir();
    }
    public static void resta(int num1,int den1,int num2,int den2){
        numerador= ((den1*num2)-den2*num1);
        denominador=(den1*den2);
        reducir();
    }
    public static void multiplicacion(int num1,int den1,int num2,int den2){
        numerador= (num2*num1);
        denominador=(den1*den2);
        reducir();
    }
    public static void division(int num1,int den1,int num2,int den2){
        numerador= (den2*num1);
        denominador=(den1*num2);
        reducir();
    }
    public String Representacion(int num1,int den1){
       double resultado;
       numerador= num1;
       denominador= den1;
        reducir();
        resultado = numerador/denominador ;
        return "El resultado de la representacion de la fraccion: "+num1+"/"+den1+" (su forma reducida: "
                +numerador+"/"+denominador+") es: "+resultado;
    }
    public String ElegirRespresentacion(int num,int den1,int opc){
       double resultado;
       numerador= num;
       denominador= den1;
        reducir();
        resultado = numerador/denominador ;
        switch(opc) {
            case 1:
                    DecimalFormat d1 = new DecimalFormat("#0.0");
                    return"El resultado de la representacion de la fraccion: "+num+"/"+den1+" es: "+d1.format(resultado) ;
            case 2:
                    DecimalFormat d2 = new DecimalFormat("#0.00");
                    return"El resultado de la representacion de la fraccion: "+num+"/"+den1+" es: "+d2.format(resultado) ;
            case 3:
                    DecimalFormat d3 = new DecimalFormat("#0.000");
                    return"El resultado de la representacion de la fraccion: "+num+"/"+den1+" es: "+d3.format(resultado) ;    
        }
        return "El resultado de la representacion de la fraccion: "+num+"/"+den1+" es: "+resultado;
    }
    
    @Override
    public String toString() {
        return "Fraccion Reducida Resultante: "+getNumerador()+"/"+getDenominador();
    }    
}
public class PruebaRacional {
    public static void main(String[] args) {
        Scanner teclado= new Scanner(System.in);
        int opc;
        Racional racional1=new Racional(10,8);
        System.out.println(racional1.toString());
        System.out.println("Suma de fracciones");
        Racional.suma(2,2,3,4);
        System.out.println(racional1.toString());
        System.out.println("Resta de fracciones");
         Racional.resta(2,2,3,4);
        System.out.println(racional1.toString());
        System.out.println("Multiplicacion de fracciones");
        Racional.multiplicacion(2,2,3,4);
        System.out.println(racional1.toString());
        System.out.println("Division de fracciones");
        Racional.division(2,2,3,4);
        System.out.println(racional1.toString());
        System.out.println(racional1.Representacion(10,15 ));
        System.out.println("Elija  el numero de digitos que desea  que se presenten en su resultador :\n1.\n2.\n3."
                + "\nPresione 0 si desea todos los decimales  ");opc = teclado.nextInt();
        System.out.println(racional1.ElegirRespresentacion(25, 6, opc));
    }
    
}
